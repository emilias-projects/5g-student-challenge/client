import asyncio
import signal
import numpy as np
import websockets
from PIL import Image as im
import cv2

def rescaleFrame(frame, scale = 0.75):
    width = int(frame.shape[1] * scale)
    height = int(frame.shape[0] * scale)
    dimensions = (width, height)
    return cv2.resize(frame, dimensions, interpolation = cv2.INTER_CUBIC)

def largest(arr):
    ans = max(arr)
    index = arr.index(ans)
    return [index, ans]

async def client():
    uri = "ws://192.168.8.103:8765"
    async with websockets.connect(uri) as websocket:
        last_position = 0
        from_left = False
        from_right = False
        last_left = False
        last_right = False
        counter = 0

        # Process messages received on the connection.
        async for message in websocket:
            frame = np.frombuffer(message)
            frame = np.reshape(frame, (24, 32))
            frame = frame / 30

            rgb_preview = np.dstack([frame, frame, frame])

            frame *= 255

            rgb = np.dstack([frame, frame, frame])

            ret, threshCam = cv2.threshold(rgb, 220, 255, cv2.THRESH_BINARY)
            flipedImage = cv2.flip(threshCam, 0)
            resizedFrame = rescaleFrame(flipedImage, 15)

            values = [
              get_partition((0, 0), (8 * 15, 24 * 15), resizedFrame),
              get_partition((8 * 15, 0), (16 * 15, 24 * 15), resizedFrame),
              get_partition((16 * 15, 0), (24 * 15, 24 * 15), resizedFrame),
              get_partition((24 * 15, 0), (32 * 15, 24 * 15), resizedFrame)
            ]

            # get amount of people
            position = largest(values)

            if int(position[1]) > 7: # 5

              if position[0] == 3 and not from_left and not last_left and not from_right and not last_right:
                print("Starting from left")
                last_left = True
                from_left = True
              elif position[0] == 2 and from_left and last_left and not from_right and not last_right:
                last_left = False
              elif position[0] == 0 and from_left and not last_left and not from_right and not last_right:
                print("Arrived right")
                counter += 1
                print(counter)
                last_left = False
                from_left = False
                from_right = True
                last_right = True
              elif position[0] == 3 and not from_left and not last_left and not from_right and not last_right:
                print("Starting from right")
                last_right = True
                from_right = True
              elif position[0] == 1 and not from_left and not last_left and from_right and last_right:
                last_right = False
              elif position[0] == 3 and not from_left and not last_left and from_right and not last_right:
                print("Arrived left")
                counter -= 1
                print(counter)
                last_left = True
                from_left = True
                from_right = False
                last_right = False
              elif position[0] == 3 and not from_left and not last_left and from_right and last_right:
                print("New person from left")
                from_left = True
                last_left = True
                last_right = False
                from_right = False
              elif position[0] == 0 and from_left and last_left and not from_right and not last_right:
                print("New person from right")
                from_left = False
                last_left = False
                from_right = True
                last_right = True

              last_position = position[0]

            blank = np.zeros((500, 500, 3))
            count = cv2.putText(blank, str(counter), (225, 255), cv2.FONT_HERSHEY_COMPLEX_SMALL, 5, (0, 255, 0), 5)
            cv2.imshow("counter", count)
            blank = np.zeros((500, 500, 3))

            fliped_preview = cv2.flip(rgb_preview, 0)
            upscaled_preview = rescaleFrame(fliped_preview, 15)
            cv2.imshow("preview", upscaled_preview)

            cv2.imshow("Thresholed", resizedFrame)
            key = cv2.waitKey(1)
            if key & 0xFF == ord('q'):
                break

def get_partition(pt1, pt2, frame):
  mask = np.zeros(frame.shape[:2], dtype=np.uint8)
  test = cv2.rectangle(mask, pt1, pt2, 255, -1)
  section_mean = cv2.mean(frame, mask)
  return section_mean[0]


asyncio.run(client())